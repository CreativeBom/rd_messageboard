-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2016-01-23 08:39:32
-- 伺服器版本: 10.1.9-MariaDB
-- PHP 版本： 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `rd_messageboard`
--

-- --------------------------------------------------------

--
-- 資料表結構 `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `rd_member`
--

CREATE TABLE `rd_member` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `account` varchar(20) NOT NULL,
  `pw` varchar(41) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `rd_member`
--

INSERT INTO `rd_member` (`id`, `name`, `email`, `account`, `pw`, `time`) VALUES
(1, '王醫師', 'wang@bestivf.com.tw', 'wang', 'wang', '2016-01-20 13:45:12');

-- --------------------------------------------------------

--
-- 資料表結構 `rd_messageboard`
--

CREATE TABLE `rd_messageboard` (
  `id` int(11) NOT NULL,
  `mclass` varchar(32) NOT NULL,
  `title` varchar(128) NOT NULL,
  `content` text NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `secret` tinyint(1) NOT NULL DEFAULT '0',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `rd_messageboard`
--

INSERT INTO `rd_messageboard` (`id`, `mclass`, `title`, `content`, `name`, `email`, `phone`, `secret`, `time`) VALUES
(1, 'common', '想請問試管嬰兒和精卵輸卵管植入術有何不同？', '如題:想請問試管嬰兒和精卵輸卵管植入術有何不同？', '王大大', 'abc@123.com', '0987654321', 1, '2016-01-16 17:01:01'),
(2, 'common', '什麼情況是需要進入試管嬰兒治療?', ':)', '林小小', 'qwe@gmail.com', '0987654333', 0, '2016-01-16 20:46:55'),
(3, 'normal', 'XD', 'XD', 'XD', 'XD', 'D', 0, '2016-01-17 02:25:22'),
(4, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:25:32'),
(5, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:26:00'),
(7, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:28:22'),
(8, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:29:58'),
(9, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:31:57'),
(10, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:32:29'),
(12, 'normal', '感謝醫生的照顧', '真的很開心可以遇到像你這麼好的醫生', '王大蠻', 'zxc@gmail.com', '0912345678', 0, '2016-01-17 02:38:04'),
(13, 'normal', '感謝醫生的照顧2', ':) thx!', '王大蠻', 'zxc@gmail.com', '0912345678', 0, '2016-01-17 02:40:06'),
(14, 'normal', '王大蠻', 'XD', 'XD', 'zxc@gmail.com', '0912345678', 0, '2016-01-20 22:13:27'),
(15, 'normal', '王大蠻', 'XD', 'XD', 'zxc@gmail.com', '0912345678', 0, '2016-01-20 22:14:52'),
(16, 'normal', '建立留言內容來感謝醫生', '如標題。', '留言先生', 'zxc@gmail.com', '0912345678', 1, '2016-01-20 23:48:26'),
(17, 'normal', '建立留言內容來感謝醫生', '如標題。', '留言先生', 'zxc@gmail.com', '0912345678', 1, '2016-01-20 23:48:33'),
(18, 'normal', '建立留言內容來感謝醫生', '如標題。', '留言先生', 'zxc@gmail.com', '0912345678', 1, '2016-01-20 23:48:50'),
(19, 'normal', 'asdasd', 'asdasdasd', 'asd', 'asd@asd.com', '0987654321', 1, '2016-01-22 01:19:45'),
(20, 'normal', 'asdasd', 'asdasdasd', 'asd', 'asd@asd.com', '0987654321', 1, '2016-01-22 01:22:10'),
(21, 'normal', 'asdasd', 'asdasdasd', 'asd', 'asd@asd.com', '0987654321', 1, '2016-01-22 01:22:18'),
(22, 'normal', 'asdasd', 'asdasdasd', 'asd', 'asd@asd.com', '0987654321', 1, '2016-01-22 01:22:50'),
(24, 'normal', '感謝醫生的照顧3', '由衷感謝。', '呵呵先生', '123@GMAIL.COM', '0987654321', 0, '2016-01-23 01:25:28'),
(25, 'normal', '王大蠻', '感謝照顧 Orz', '王大蠻', 'zxc@gmail.com', '0912345678', 0, '2016-01-23 01:28:45');

-- --------------------------------------------------------

--
-- 資料表結構 `rd_messagereply`
--

CREATE TABLE `rd_messagereply` (
  `id` int(11) NOT NULL,
  `mb_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `rd_messagereply`
--

INSERT INTO `rd_messagereply` (`id`, `mb_id`, `content`, `time`) VALUES
(1, 1, '記得好好休息。', '2016-01-16 14:25:26'),
(2, 3, 'bnm', '2016-01-21 17:09:57'),
(3, 3, 'bnm', '2016-01-21 17:10:24'),
(4, 3, 'bnm', '2016-01-21 17:10:29'),
(5, 3, 'bnm', '2016-01-21 17:10:40'),
(6, 3, 'bnm', '2016-01-21 17:11:34'),
(7, 1, '記得好好休息。 :)', '2016-01-21 17:30:34'),
(8, 1, '記得好好休息。 :目', '2016-01-21 17:32:30'),
(9, 1, '記得好好休息。 :目', '2016-01-21 17:32:48'),
(10, 1, '記得好好休息。 :目', '2016-01-21 17:32:55'),
(11, 1, '記得好好休息。 :目', '2016-01-21 17:33:05'),
(12, 1, '記得好好休息。 :目', '2016-01-21 17:33:15'),
(13, 1, '記得好好休息。 :))', '2016-01-21 17:34:08'),
(14, 1, '記得好好休息。 :)))', '2016-01-21 17:36:52'),
(15, 1, '記得好好休息。', '2016-01-21 17:38:54'),
(16, 1, '記得好好休息。', '2016-01-21 17:38:58'),
(17, 1, '記得好好休息。', '2016-01-22 16:17:07'),
(18, 3, 'bnm', '2016-01-22 16:30:00'),
(19, 1, '記得好好休息。 ><"', '2016-01-22 16:46:32'),
(20, 25, 'XD', '2016-01-22 18:36:30'),
(21, 1, '記得好好休息。 ><"', '2016-01-23 04:59:00'),
(22, 1, '記得好好休息。 ><"', '2016-01-23 05:01:00'),
(23, 1, '記得好好休息。 ><"', '2016-01-23 05:03:34'),
(24, 1, '記得好好休息。 ><"', '2016-01-23 05:11:47'),
(25, 1, '記得好好休息。 ><"', '2016-01-23 05:26:24'),
(26, 1, '記得好好休息。 ><"', '2016-01-23 05:29:35'),
(27, 1, '記得好好休息。 ><"', '2016-01-23 05:31:48'),
(28, 1, '記得好好休息。 ><"', '2016-01-23 05:32:23'),
(29, 1, '記得好好休息。 ><"', '2016-01-23 05:41:59');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- 資料表索引 `rd_member`
--
ALTER TABLE `rd_member`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `rd_messageboard`
--
ALTER TABLE `rd_messageboard`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `rd_messagereply`
--
ALTER TABLE `rd_messagereply`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `rd_member`
--
ALTER TABLE `rd_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `rd_messageboard`
--
ALTER TABLE `rd_messageboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- 使用資料表 AUTO_INCREMENT `rd_messagereply`
--
ALTER TABLE `rd_messagereply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
