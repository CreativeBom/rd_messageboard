<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'libraries/PHPMailer/PHPMailerAutoload.php');//PHPMailerAutoload//class.phpmailer
class Pages extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
		$this->load->model('replys_model');
		$this->load->library('session');		
	}

	public function WebsiteURL(){
		return "http://bestivf.com.tw/rd_messageboard";
		//return "http://localhost/rd_messageboard";
	} 

	public function index()
	{
		if ( ! file_exists('application/views/pages/home.php'))
		{
			show_404();// 哇勒!我們沒有這個頁面!
		}				
		
		$data['title'] = "Home";
		$data['news_item_normal'] = $this->news_model->get_all_normal_news('all');
		$data['news_item_common'] = $this->news_model->get_news_mclass('common',5,0);
		$data['news_item_normal_by_pages'] = $this->news_model->get_news_all(10,0);

		if (empty($data['news_item_normal']))//||empty($data['news_item_common'])
		{
			//show_404();//目前沒有資料
		}
		
		$this->load->library('pagination'); //固定設定移到Config/pagination.php
		$config['total_rows'] = count($data['news_item_normal']);
		$config['base_url'] = $this->WebsiteURL().'/index.php/pages/messagespage/A-';
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();


		//$data['title'] = $data['news_item_normal']['title'];
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		$this->load->view('pages/home', $data);
		$this->load->view('templates/footer', $data);
	}

	public function messagespage($classpages = "A-0")
	{
		if ( ! file_exists('application/views/pages/home.php'))
		{
			show_404();// 哇勒!我們沒有這個頁面!
		}				
		
		$data['title'] = '一般留言'; // 第一個字母大寫
				
		$mclass = 'all';
		$pages = 0;
		if($classpages){
			$str = explode("-",$classpages);
			switch ($str[0]) {
				case 'A':
					$mclass = 'all';
					break;
				case 'B':
					$mclass = '1';
					break;
				case 'C':
					$mclass = '2';
					break;
				case 'D':
					$mclass = '3';
					break;
				case 'E':
					$mclass = '4';
					break;
				case 'F':
					$mclass = '5';
					break;
				default:
					$mclass = 'all';
					break;
			}
			$pages = $str[1];
		}

		$data['news_item_normal'] = $this->news_model->get_all_normal_news($mclass);
		$data['news_item_normal_by_pages'] = $this->news_model->get_news_all_mclass($mclass,10,$pages);
		
		if (empty($data['news_item_normal']))//||empty($data['news_item_common'])
		{
			//show_404();//目前沒有資料
		}

		if ($pages == 0){
			$data['news_item_common'] = $this->news_model->get_news_mclass('common',5,0);
		}
		
		$this->load->library('pagination'); //固定設定移到Config/pagination.php
		$config['total_rows'] = count($data['news_item_normal']);
		$config['base_url'] = $this->WebsiteURL().'/index.php/pages/messagespage/'.$str[0]."-";
		$this->pagination->initialize($config); 
		$data['pagination'] = $this->pagination->create_links();

		$this->load->view('templates/header_inside', $data);
		$this->load->view('templates/menu_inside', $data);
		$this->load->view('pages/messagespage', $data);
		$this->load->view('templates/footer_inside', $data);
	}

	public function view($id)
	{
		if ( ! file_exists('application/views/pages/home.php'))
		{
			show_404();// 哇勒!我們沒有這個頁面!
		}				
				
		$data['news_item_id'] = $this->news_model->get_news_id($id);
		$data['replys_item_mid'] = $this->replys_model->get_replys_mid($id);
		$data['title'] = ucfirst($data['news_item_id']['title']); // 第一個字母大寫

		if (empty($data['news_item_id']))//||empty($data['news_item_common'])
		{
			//show_404();//目前沒有資料
		}

		$this->load->view('templates/header_inside', $data);
		$this->load->view('templates/menu_inside', $data);
		$this->load->view('pages/view', $data);
		$this->load->view('templates/footer_inside', $data);
	}

	public function create($message = 'message')
	{
		$this->load->helper('form');
		$this->load->helper('captcha');
		$this->load->library('form_validation');
		$this->form_validation->set_message('required', '尚未填寫');
		$this->form_validation->set_message('valid_email', '%s 填寫錯誤');
		$this->form_validation->set_message('integer', '%s 需要是數字');
		$this->form_validation->set_message('exact_length', '%s 長度不正確');
		$this->form_validation->set_message('max_length', '%s 超過規定長度');
		$this->form_validation->set_message('callback_captcha_check', '%s 錯誤');

		$data['title'] = '新增留言';
		
		$this->form_validation->set_rules('name', '姓名', 'required|max_length[20]');
		$this->form_validation->set_rules('phone', '連絡電話', 'required|exact_length[10]|integer');
		$this->form_validation->set_rules('email', '電子信箱', 'required|valid_email');
		$this->form_validation->set_rules('title', '標題', 'required|max_length[30]');
		$this->form_validation->set_rules('text', '內文', 'required|max_length[1073741823]');
		$this->form_validation->set_rules('captcha', '驗證碼', 'callback_captcha_check');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header_inside', $data);
			$this->load->view('templates/menu_inside', $data);
			$data['cap']=$this->captcha_img();
			$this->load->view('pages/create', $data);
			$this->load->view('templates/footer_inside', $data);
		}
		else
		{
			$this->load->view('templates/header_inside', $data);
			$this->load->view('templates/menu_inside', $data);
			$data['create']=$this->news_model->set_news();
			$this->load->view('pages/success', $data);
			$this->load->view('templates/footer_inside', $data);
		}
	}

	public function captcha_check($str)
	{
		if ($str != $this->session->userdata('captcha'))
		{
			$this->form_validation->set_message('captcha_check', '%s 錯誤');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	private function captcha_img() 
    {
        $pool = '0123456789';
        $word = '';
        for ($i = 0; $i < 4; $i++){
            $word .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }
        $this->session->set_userdata('captcha', $word);
        
        $vals = array(
            'word'  => $word,
            'img_path'  => './captcha/',
            'img_url'  => $this->WebsiteURL().'/captcha/',
            'expiration' => 7200
            );
        $cap = create_captcha($vals);
        return $cap['image'];
    }

	public function admin($message = 'message',$classpages = "A-0")
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('account', 'account', 'required');
		$this->form_validation->set_rules('pw', 'password', 'required');

		$data['title'] = '後臺管理';
		if ( ! file_exists('application/views/pages/home.php'))
		{
			show_404();// 哇勒!我們沒有這個頁面!
		}				

		if($message == 'logout'){
			$this->session->unset_userdata('session_id');
			header("Location: ".$this->WebsiteURL());
		}

		$mclass = 'all';
		$pages = 0;
		if($classpages){
			$str = explode("-",$classpages);
			switch ($str[0]) {
				case 'A':
					$mclass = 'all';
					break;
				case 'B':
					$mclass = '1';
					break;
				case 'C':
					$mclass = '2';
					break;
				case 'D':
					$mclass = '3';
					break;
				case 'E':
					$mclass = '4';
					break;
				case 'F':
					$mclass = '5';
					break;
				default:
					$mclass = 'all';
					break;
			}
			$pages = $str[1];
		}

		if($this->session->userdata('session_id')!= null){
			$data['news_item_normal'] = $this->news_model->get_all_admin_normal_news($mclass);
			$data['news_item_common'] = $this->news_model->get_news_admin_mclass('common',20,0);
			$data['news_item_normal_by_pages'] = $this->news_model->get_news_admin_all($mclass,10,0);
			
			for ( $i =0;$i<count($data['news_item_common']);$i++){
				$data['replys_item_common_mid'][$i] = $this->replys_model->get_replys_mid($data['news_item_common'][$i]['id']);
			}
			for ( $i =0;$i<count($data['news_item_normal_by_pages']);$i++){
				$data['replys_item_mid'][$i] = $this->replys_model->get_replys_mid($data['news_item_normal_by_pages'][$i]['id']);
			}			
			
			if (empty($data['news_item_normal']))//||empty($data['news_item_common'])
			{
				//show_404();//目前沒有資料
			}
			
			$this->load->library('pagination'); //固定設定移到Config/pagination.php
			$config['total_rows'] = count($data['news_item_normal']);
			$config['base_url'] = $this->WebsiteURL().'/index.php/pages/adminpage/'.$str[0]."-";
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
		
			$this->load->view('templates/header_inside', $data);
			$this->load->view('templates/menu_inside', $data);
			$this->load->view('pages/admin_home', $data);
			$this->load->view('templates/footer_inside', $data);
		}
		else if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header_inside', $data);
			$this->load->view('templates/menu_inside', $data);
			$this->load->view('pages/admin');
			$this->load->view('templates/footer_inside', $data);
		}
		else 
		{
			$login = $this->news_model->RD_login();
			if(!empty($login)){
				$data['login'] = '您已登入成功';
				$newdata = array(
                   'session_id'  => $this->news_model->RD_login(),
                   'logged_in' => TRUE
               	);
				$this->session->set_userdata($newdata);
				header("Location: ".$this->WebsiteURL()."/index.php/pages/admin/");
			}
			else {
				$data['login'] = '您登入失敗';
				//這邊需要修正，登入失敗要顯示一些內容。
				header("Location: ".$this->WebsiteURL()."/index.php/pages/admin/login");
			}
			$this->load->view('templates/header_inside', $data);
			$this->load->view('templates/menu_inside', $data);
			$this->load->view('pages/login_success', $data);
			$this->load->view('templates/footer_inside', $data);
		}
	}

	public function adminpage($classpages)
	{
		$mclass = 'all';
		$pages = 0;
		if($classpages){
			$str = explode("-",$classpages);
			switch ($str[0]) {
				case 'A':
					$mclass = 'all';
					break;
				case 'B':
					$mclass = '1';
					break;
				case 'C':
					$mclass = '2';
					break;
				case 'D':
					$mclass = '3';
					break;
				case 'E':
					$mclass = '4';
					break;
				case 'F':
					$mclass = '5';
					break;
				default:
					$mclass = 'all';
					break;
			}
			$pages = $str[1];
		}
		$data['title'] = '一般留言';
		if($this->session->userdata('session_id')== null)
		{
			header("Location: ".$this->WebsiteURL()."/index.php/pages/admin/login");
			//show_404();// 哇勒!我們沒有這個頁面! //這樣後面還會執行嗎?
		}				
		else {
			if($this->session->userdata('session_id')!= null){//資料庫要只Loading範圍內的，以降低讀取時間
				$data['news_item_normal'] = $this->news_model->get_all_admin_normal_news($mclass);
				$data['news_item_normal_by_pages'] = $this->news_model->get_news_admin_all($mclass,10,$pages);
				
				
				for ( $i =0;$i<count($data['news_item_normal_by_pages']);$i++){
					$data['replys_item_mid'][$i] = $this->replys_model->get_replys_mid($data['news_item_normal_by_pages'][$i]['id']);
				}

			}
			else {
				$data['news_item_normal'] = $this->news_model->get_all_normal_news();
				$data['news_item_normal_by_pages'] = $this->news_model->get_news_mclass('normal',10,$pages);
			}
			
			if (empty($data['news_item_normal']))//||empty($data['news_item_common'])
			{
				//show_404();
			}

			if ($pages == 0){
				$data['news_item_common'] = $this->news_model->get_news_mclass('common',20,0);

				for ( $i =0;$i<count($data['news_item_common']);$i++){
					$data['replys_item_common_mid'][$i] = $this->replys_model->get_replys_mid($data['news_item_common'][$i]['id']);
				}
			}
			
			$this->load->library('pagination'); //固定設定移到Config/pagination.php
			$config['total_rows'] = count($data['news_item_normal']);
			$config['base_url'] = $this->WebsiteURL().'/index.php/pages/adminpage/'.$str[0]."-";
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();

			$this->load->view('templates/header_inside', $data);
			$this->load->view('templates/menu_inside', $data);
			$this->load->view('pages/admin_messagespage', $data);
			$this->load->view('templates/footer_inside', $data);
		}
	}

	public function adminview($id = null)
	{
		if($this->session->userdata('session_id')== null)
		{
			header("Location: ".$this->WebsiteURL()."/index.php/pages/admin/login");
			//show_404();// 哇勒!我們沒有這個頁面! //這樣後面還會執行嗎?
		}
		else{
			$data['news_item_id'] = $this->news_model->get_news_id($id);
			$data['replys_item_mid'] = $this->replys_model->get_replys_mid($id);
			//print_r($data['news_item_id']);
			$data['title'] = ucfirst($data['news_item_id']['title']); // 第一個字母大寫

			if (empty($data['news_item_id']))//||empty($data['news_item_common'])
			{
				show_404();
			}

			$this->load->helper('form');
			$this->load->library('email');
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '未填寫');
			$this->form_validation->set_rules('text', '回覆內容', 'required');
			$this->form_validation->set_rules('news_content', '問題', 'required');

			if ($this->form_validation->run() === FALSE)
			{
				$this->load->view('templates/header_inside', $data);
				$this->load->view('templates/menu_inside', $data);
				$this->load->view('pages/admin_view', $data);
				$this->load->view('templates/footer_inside', $data);
			}
			else
			{
				$this->load->view('templates/header_inside', $data);
				$this->load->view('templates/menu_inside', $data);
				$this->news_model->set_reply($data['news_item_id']['id']);
				
				$secret = 0 ;
				if($this->input->post('secret')=="yes"){
					$secret = 1;
				}
				else if($this->input->post('secret')=="no"){
					$secret = 0;
				}

				if($secret==1){
					$this->sendemail($data['news_item_id']['email'],$this->input->post('text'),$data['news_item_id']['name']);
				}
				
				$data['title']="回覆留言";
				$this->load->view('pages/admin_success',$data);
				$this->load->view('templates/footer_inside', $data);
			}

		}
	}

	public function delete($id = null)
	{
		$data['title'] = '刪除留言';
		if($this->session->userdata('session_id')== null)
		{
			header("Location: ".$this->WebsiteURL()."/index.php/pages/admin/login");
			//show_404();// 哇勒!我們沒有這個頁面! //這樣後面還會執行嗎?
		}
		else
		{
			$this->load->view('templates/header_inside', $data);
			$this->load->view('templates/menu_inside', $data);
			$this->news_model->delete_news($id);
			$this->load->view('pages/admin_success', $data);
			$this->load->view('templates/footer_inside', $data);
		}
	}

	public function Sendemail($email = null,$content = null,$name = null){
	  	if($email&&$content){
	  		$mail= new PHPMailer();                          //建立新物件  
		  	$mail->IsSMTP();                                    //設定使用SMTP方式寄信  
		    $mail->SMTPAuth = true;                        //設定SMTP需要驗證  
		    $mail->SMTPSecure = "ssl";                    // Gmail的SMTP主機需要使用SSL連線  
		    $mail->Host = "smtp.gmail.com";             //Gamil的SMTP主機  
		    $mail->Port = 465;    
			$mail->CharSet = "utf-8"; //設定郵件編碼
			$mail->Encoding = "base64";
			$mail->Username = 'bestivf.tw@gmail.com'; //設定驗證帳號
			$mail->Password = 'bestivf0000'; //設定驗證密碼
			$mail->From = 'bestivf.tw@gmail.com'; //設定寄件者信箱
			$mail->IsHTML(true); //設定郵件內容為HTML  
			$mail->FromName = 'Bestivf'; //設定寄件者姓名
			$mail->Subject = '您的私密留言回覆'; //設定郵件標題
			$mail->AddAddress($email); //設定收件者郵件及名稱
			$Email_Body="Dear ".$name." 先生/小姐 您好,<br><br>
			您在本中心留言版上選擇了私密留言，此信件是由系統發出之信件，請勿直接回覆，感謝您的配合。<br>
			<br>
			--------------留言回覆內容------------<br>
			<pre>".$content."</pre><br>
			<br>			
			----------------------------------------------------------------------------------------------------------------<br>
			若有其他疑問，歡迎至本留言版 http://bestivf.com.tw/rd_messageboard/index.php 詢問。<br>
			再次感謝您的留言。<br>
			<br>
			Regards,<br>
			Bestivf團隊<br>
			";
			
			$mail->Body =($Email_Body);
			//$mail->Send();

		   	if(!$mail->Send()){  
		        echo "Error: " . $mail->ErrorInfo;  
		    }else{  
		        //echo "<b>您好!已收到您的留言，會盡快回覆</b>";  
		    }  
	  	}
	  	
	}

}