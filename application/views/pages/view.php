	<div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <?php 
        include("RDheader.php"); 
      ?>

      <div class="page-header">
        
        <!--需要判斷留言類型，才會顯示正確顏色-->
        <?php
        	if($news_item_id['mclass'] != "common"){
        		echo "<h2><span class=\"label label-info\">一般留言</span>";
        	}
        	else if ($news_item_id['mclass'] == "common"){
        		echo "<h2><span class=\"label label-warning\">常見問題</span>";
        	}
        ?>
        
        <input style="float:right;" type ="button" class="btn btn-link" onclick="history.back()" value="回上一頁"></input>
        </h2><h2>&nbsp;<?php echo $news_item_id['title']?></h2>
      </div>

      <div><!--class="col-md-6"-->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                <b><?php 
                	if($news_item_id['secret']==0){
                		echo mb_substr($news_item_id['name'], 0,1,"utf-8")." 先生/小姐";
                	}
                	else {
                		echo "匿名 先生/小姐";
                	}
                ?></b>&nbsp;&nbsp;
                於&nbsp;<?php echo $news_item_id['time'] ?>&nbsp;提問
              </h3>
            </div>
            <div class="panel-body">
              <pre style="font-size:16px;background:white; border: none; padding:0px;"><?php echo $news_item_id['content'] ?></pre>
            </div>
          </div>
          
          <?php if ($replys_item_mid['mb_id'] == $news_item_id['id']): ?>
          <!--需要判斷醫生有沒有回覆，才會顯示下面-->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
              	<b>醫生回覆</b>
              </h3>
            </div>
            <div class="panel-body">
              <pre style="font-size:16px;background:white; border: none; padding:0px;"><?php echo $replys_item_mid['content'] ?></pre>
            </div>
          </div>
          <?php endif; ?>

       </div>

      <hr>
      
      <footer>
        <p><strong><center>&copy; 2015 Bestivf, Inc.</center></strong></p>
      </footer>

    </div> <!-- /container -->