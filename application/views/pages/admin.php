    <link href="../../../bootstrap/signin.css" rel="stylesheet">
    <div class="container">
    <br><br>
      <form class="form-signin" role="form" method="post" accept-charset="utf-8" action="../admin/login">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Account</label>
        <input type="input" id="inputAccount" name="account" class="form-control" placeholder="Account" value="<?php echo set_value('account'); ?>" required autofocus>
        <!--input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus-->
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="pw" class="form-control" placeholder="Password"  required>
        <!--div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div-->
        <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->