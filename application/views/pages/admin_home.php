    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <div class="well">
        <h2>留言板 - 管理區</h2>
      </div>

      <div class="page-header">
        <h1><span class="label label-warning">常見問題</span></h1>
      </div>

      <div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
                <th>類型</th>
                <th>是否回覆</th>
                <th>刪除?</th>
              </tr>
            </thead>
            <tbody>
			<?php foreach ($news_item_common as $key => $rows): ?>
              <tr>
                <td class="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-6"><a href="../../../index.php/pages/adminview/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-1"><?php echo $rows['name']; ?></td>
                <td class="col-md-2"><?php echo $rows['time']; ?></td>
                <td class="col-md-1">
                  <?php if($rows['secret']==0) {
                      echo "公開留言";
                    }
                    else if($rows['secret']==1) {
                      echo "私密留言";
                    }
                  ?>
                </td>
                <td class="col-md-1"><?php 
                  echo (isset($replys_item_common_mid[$key])) ?  '<span class="label label-default">已回覆</span>' : '<a href="../../../index.php/pages/adminview/'.$rows['id'].'" class="label label-danger">未回覆</a>'; 
                ?></td>
                <td class="col-md-1"><span class="glyphicon glyphicon-trash" aria-hidden="true" data-toggle="modal" data-target="#myModal<?php echo $rows['id'] ;?>"></span></td>
              </tr>
              <!-- Modal -->
              <div id="myModal<?php echo $rows['id'] ;?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h3 class="modal-title"><b>即將刪除一則留言</b></h3>
                    </div>
                    <div class="modal-body">
                      <h3><p><?php echo "欲刪除 \"".$rows['title']."\" 此一留言";?></p>
                      <br>
                      <p>若刪除後 ，留言內容將完全刪除</p>
                      <p>請確認是否要刪除?</p></h3>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
                      <a href="../delete/<?php echo $rows['id'] ?>" class="btn btn-success">確認</a>
                    </div>
                  </div>

                </div>
              </div>
            <?php endforeach ?>
            </tbody>
          </table>
       </div>

      <div class="page-header">
        <h1><span class="label label-info">一般留言</span></h1>
      </div>
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="../adminpage/A-0">全部</a></li>
        <li role="presentation"><a href="../adminpage/B-0">療程相關</a></li>
        <li role="presentation"><a href="../adminpage/C-0">針劑使用相關</a></li>
        <li role="presentation"><a href="../adminpage/D-0">營養補充相關 </a></li>
        <li role="presentation"><a href="../adminpage/E-0">冷凍胚胎相關</a></li>
        <li role="presentation"><a href="../adminpage/F-0">懷孕後相關</a></li>
      </ul>
		<div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
                <th>類型</th>
                <th>是否回覆</th>
                <th>刪除?</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($news_item_normal_by_pages as $key => $rows): ?>
              <tr>
                <td class="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-6"><a href="../../../index.php/pages/adminview/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-1"><?php echo $rows['name']; ?></td>
                <td class="col-md-2"><?php echo $rows['time']; ?></td>
                <td class="col-md-1"><?php if($rows['secret']==0) {
                      echo "公開留言";
                    }
                    else if($rows['secret']==1) {
                      echo "私密留言";
                    }
                  ?>
                </td>
                <td class="col-md-1"><?php 
                  echo (isset($replys_item_mid[$key])) ?  '<span class="label label-default">已回覆</span>' : '<a href="../../../index.php/pages/adminview/'.$rows['id'].'" class="label label-danger">未回覆</a>'; 
                ?>
                </td>
                <td class="col-md-1"><span class="glyphicon glyphicon-trash" aria-hidden="true" data-toggle="modal" data-target="#myModal<?php echo $rows['id'] ;?>"></span></td>
              </tr>
              <!-- Modal -->
              <div id="myModal<?php echo $rows['id'] ;?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h3 class="modal-title"><b>即將刪除一則留言</b></h3>
                    </div>
                    <div class="modal-body">
                      <h3><p><?php echo "欲刪除 \"".$rows['title']."\" 此一留言";?></p>
                      <br>
                      <p>若刪除後 ，留言內容將完全刪除</p>
                      <p>請確認是否要刪除?</p></h3>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
                      <a href="../delete/<?php echo $rows['id'] ?>" class="btn btn-success">確認</a>
                    </div>
                  </div>

                </div>
              </div>
            <?php endforeach ?>
            </tbody>
          </table>

      <?php //CI的分頁系統
        if (isset($pagination)){
            echo $pagination;
          }
      ?>

       </div>



      <hr>
      
      <footer>
        <p><strong><center>&copy; 2015 Bestivf, Inc.</center></strong></p>
      </footer>

    </div> <!-- /container -->