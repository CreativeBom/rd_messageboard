    <div class="container">
      <!-- Main component for a primary marketing message or call to action -->
      <?php 
        include("RDheader.php"); 
      ?>
      

      <div class="page-header">
        <h1><span class="label label-warning">常見問題</span></h1>
      </div>

      <div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
              </tr>
            </thead>
            <tbody>
			<?php foreach ($news_item_common as $key => $rows): ?>
              <tr>
                <td style="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-7"><a href="index.php/pages/view/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-2"><?php 
                  if($rows['secret']==0){
                    echo mb_substr($rows['name'], 0,1,"utf-8")." 先生/小姐";
                  }
                  else {
                    echo "匿名 先生/小姐";
                  }
                ?></td>
                <td class="col-md-2"><?php echo $rows['time'] ?></td>
              </tr>
            <?php endforeach ?>
            </tbody>
          </table>
       </div>

      <div class="page-header">
        <h1><span class="label label-info">一般留言</span></h1>
      </div>
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="index.php/pages/messagespage/A-0">全部</a></li>
        <li role="presentation"><a href="index.php/pages/messagespage/B-0">療程相關</a></li>
        <li role="presentation"><a href="index.php/pages/messagespage/C-0">針劑使用相關</a></li>
        <li role="presentation"><a href="index.php/pages/messagespage/D-0">營養補充相關</a></li>
        <li role="presentation"><a href="index.php/pages/messagespage/E-0">冷凍胚胎相關</a></li>
        <li role="presentation"><a href="index.php/pages/messagespage/F-0">懷孕後相關</a></li>
      </ul>
		<div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($news_item_normal_by_pages as $key => $rows): ?>
              <tr>
                <td style="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-7"><a href="index.php/pages/view/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-2"><?php 
                  if($rows['secret']==0){
                    echo mb_substr($rows['name'], 0,1,"utf-8")." 先生/小姐";
                  }
                  else {
                    echo "匿名 先生/小姐";
                  }
                ?></td>
                <td class="col-md-2"><?php echo $rows['time'] ?></td>
              </tr>
            <?php endforeach ?>
            </tbody>
          </table>

      <?php //CI的分頁系統
        if (isset($pagination)){
            echo $pagination;
          }
      ?>

       </div>

      <hr>
      
      <footer>
        <p><strong><center>&copy; 2015 Bestivf, Inc.</center></strong></p>
      </footer>

    </div> <!-- /container -->