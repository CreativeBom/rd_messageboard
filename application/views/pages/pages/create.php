	<div class="container">


      <!-- Main component for a primary marketing message or call to action -->
      <?php 
        include("RDheader.php"); 
      ?>

      <div class="page-header">
		<h2><center>建立留言內容</center></h2>
	  </div>

		<!--?php echo validation_errors(); ?-->
		<form class="form-horizontal" role="form" method="post" accept-charset="utf-8" action="../create/message" />
			<div class="form-group">
			<label class="control-label col-sm-2"  for="name">姓名</label>
			<div class="col-sm-4">
			<input type="input" class="form-control" name="name" placeholder="請輸入您的稱呼" value="<?php echo set_value('name'); ?>"/><br /></div>
			<span style="color:red;"><?php echo form_error('name'); ?></span></div>

			<div class="form-group">
			<label class="control-label col-sm-2"  for="phone">連絡電話</label>
			<div class="col-sm-4">
			<input type="input" class="form-control" name="phone" placeholder="請輸入您的連絡電話" value="<?php echo set_value('phone'); ?>"><br /></div>
			<span style="color:red;"><?php echo form_error('phone'); ?></span></div>

			<div class="form-group">
			<label class="control-label col-sm-2"  for="email">電子信箱</label>
			<div class="col-sm-6">
			<input type="input" class="form-control" name="email" placeholder="請輸入您的Email" value="<?php echo set_value('email'); ?>" /><br /></div>
			<span style="color:red;"><?php echo form_error('email'); ?></span></div>

			<div class="form-group">
			<label class="control-label col-sm-2" for="category">分類</label>
			<div class="col-sm-2">
			<?php 
				$mclass_id = set_value('mcategory');
				if (!isset($mclass_id)){
					$mclass_id = 1;
				}
				
				$options = array(
                  '1'  => '分類1',
                  '2'    => '分類2',
                  '3'   => '分類3',
                  '4' => '分類4',
                  '5' => '分類5'
                );
                $js = 'class="form-control" id="category"';
				echo form_dropdown('mcategory', $options, $mclass_id, $js);
			?>
			</div>
			</div>

			<div class="form-group">
			<label class="control-label col-sm-2" for="title">標題</label>
			<div class="col-sm-8">
			<input type="input" class="form-control" name="title" value="<?php echo set_value('title'); ?>"/><br /></div>
			<span style="color:red;"><?php echo form_error('title'); ?></span></div>

			<div class="form-group">
			<label class="control-label col-sm-2" for="text">內文</label>
			<div class="col-sm-8">
			<textarea class="form-control" wrap="virtual|soft" rows="8" name="text"><?php echo set_value('text'); ?></textarea><br /></div>
			<span style="color:red;"><?php echo form_error('text'); ?></span></div>

			<div class="form-group">
			<label class="control-label col-sm-2"  for="captcha">驗證碼</label>
			<div class="col-sm-2">
			<input type="input" class="form-control" name="captcha" /><br /></div>
			<span style="color:red;"><?php echo $cap;?><?php echo form_error('captcha'); ?></span></div>

			<div class="form-group"> 
    		<div class="col-sm-offset-1 col-sm-10"><div class="checkbox"><center>
        	<label for="secret"><input type="checkbox" name="secret" value="1" data-toggle="modal" data-target="#myModal"> 私密留言 (若勾選此欄位 醫生將會以電子信箱方式回覆)</label></center>
      		</div></div></div>			

		  	<div class="form-group"> 
		    <div class="col-sm-offset-1 col-sm-10"><center>
		    <!-- Trigger the modal with a button -->
			<button type="submit" name="submit" class="btn btn-info btn-lg">送出留言</button>

		    </center></div></div>

		    <!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">

			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h3 class="modal-title"><b>即將建立一份新留言</b></h3>
			      </div>
			      <div class="modal-body">
			        <h3><p>若選擇 私密留言，醫生將會回覆到電子信箱中，</p><p>請確認電子信箱是否正確。</p></h3>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
			      </div>
			    </div>

			  </div>
			</div>

		</form>

	  <hr>

      <footer>
        <p><strong><center>&copy; 2015 Bestivf, Inc.</center></strong></p>
      </footer>

    </div> <!-- /container -->