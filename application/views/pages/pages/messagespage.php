    <div class="container">
      <!-- Main component for a primary marketing message or call to action -->
      <?php 
        include("RDheader.php"); 
      ?>


<?php if (!empty($news_item_common)): ?>
<div class="page-header">
        <h1><span class="label label-warning">常見問題</span></h1>
      </div>


      <div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
              </tr>
            </thead>
            <tbody>
      <?php foreach ($news_item_common as $key => $rows): ?>
              <tr>
                <td class="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-8"><a href="../../../index.php/pages/view/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-2"><?php 
                  if($rows['secret']==0){
                    echo mb_substr($rows['name'], 0,1,"utf-8")." 先生/小姐";
                  }
                  else {
                    echo "匿名 先生/小姐";
                  }
                ?></td>
                <td class="col-md-2"><?php echo $rows['time'] ?></td>
              </tr>
            <?php endforeach ?>
            </tbody>
          </table>
       </div>


<?php endif;?>
    

      <div class="page-header">
        <h1><span class="label label-info">一般留言</span></h1>
      </div>
      <ul class="nav nav-tabs" role="tablist">
        <?php 
          $str = explode("/",$_SERVER["REQUEST_URI"]);
          //echo $str[5]."<br>";
          $str = explode("-",$str[5]);
          //echo $str[0];
        ?>
        <li role="presentation" <?php if ($str[0]==="A") echo "class=\"active\""; ?>><a href="../messagespage/A-0">全部</a></li>
        <li role="presentation" <?php if ($str[0]==="B") echo "class=\"active\""; ?>><a href="../messagespage/B-0">分類1</a></li>
        <li role="presentation" <?php if ($str[0]==="C") echo "class=\"active\""; ?>><a href="../messagespage/C-0">分類2</a></li>
        <li role="presentation" <?php if ($str[0]==="D") echo "class=\"active\""; ?>><a href="../messagespage/D-0">分類3</a></li>
        <li role="presentation" <?php if ($str[0]==="E") echo "class=\"active\""; ?>><a href="../messagespage/E-0">分類4</a></li>
        <li role="presentation" <?php if ($str[0]==="F") echo "class=\"active\""; ?>><a href="../messagespage/F-0">分類5</a></li>
      </ul>


		<div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($news_item_normal_by_pages as $key => $rows): ?>
              <tr>
                <td class="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-8"><a href="../../../index.php/pages/view/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-2"><?php 
                	if($rows['secret']==0){
                		echo mb_substr($rows['name'], 0,1,"utf-8")." 先生/小姐";
                	}
                	else {
                		echo "匿名 先生/小姐";
                	}
                ?></td>
                <td class="col-md-2"><?php echo $rows['time'] ?></td>
              </tr>
            <?php endforeach ?>
            </tbody>
          </table>

      <?php //CI的分頁系統
        if (isset($pagination)){
            echo $pagination;
          }
      ?>

       </div>

      <hr>
      
      <footer>
        <p><strong><center>&copy; 2015 Bestivf, Inc.</center></strong></p>
      </footer>

    </div> <!-- /container -->