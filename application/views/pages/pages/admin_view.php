	<div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <div class="well">
        <h2>留言板 - 管理區</h2>
      </div>

      <div class="page-header">
        <h2>
        <!--需要判斷留言類型，才會顯示正確顏色-->
        <?php
        	if($news_item_id['mclass'] != "common"){
        		echo "<span class=\"label label-info\">一般留言</span>";
        	}
        	else if ($news_item_id['mclass'] == "common"){
        		echo "<span class=\"label label-warning\">常見問題</span>";
        	}
          if($news_item_id['secret'] == 1){
            echo "&nbsp;<span class=\"label label-danger\">私密留言</span>";
          }
        ?>
        &nbsp;<?php echo $news_item_id['title']?>
        <span style="float:right"><input type ="button" class="btn btn-link" onclick="history.back()" value="回上一頁"></input></span>
        </h2>
      </div>

      <div><!--class="col-md-6"-->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                <b><?php echo $news_item_id['name']." 先生/小姐"; ?></b>&nbsp;&nbsp;
                於&nbsp;<?php echo $news_item_id['time'] ?>&nbsp;提問
              </h3>
            </div>
            <div class="panel-body">
              <form class="form-horizontal" role="form" method="post" accept-charset="utf-8" action="../adminview/<?php echo $news_item_id['id'] ?>" />
              
              <div class="form-group">
              <label for="text"></label>
              <div class="col-sm-12">
              <?php if(form_error('news_content')!=null): ?>
              <div class="alert alert-danger" role="alert">
                <?php echo form_error('news_content');?>
              </div>
              <?php endif ?>

              <textarea class="form-control" wrap="virtual|soft" rows="10" name="news_content"><?php echo $news_item_id['content'] ?></textarea><br>
              <label class="control-label col-sm-1" for="category">留言分類</label>
              <div class="col-sm-2">
              <?php 
                $mclass_id = set_value('mcategory');
                if (isset($mclass_id)){
                  $mclass_id=$news_item_id['mclass'];
                }                
                $options = array(
                          '1'  => '分類1',
                          '2'    => '分類2',
                          '3'   => '分類3',
                          '4' => '分類4',
                          '5' => '分類5',
                          'common' => '常見問題'
                        );
                        $js = 'class="form-control" id="category"';
                echo form_dropdown('mcategory', $options, $mclass_id, $js);
              ?>
              </div>
              <div class="control-label col-sm-4" style="float:right;">
              <?php echo "電子信箱: ".$news_item_id['email'] ?>&nbsp;&nbsp;<?php echo "連絡電話: ".$news_item_id['phone'] ?>
              </div>
            </div>
          </div>
          
          <?php if($this->session->userdata('session_id')!= null): ?>
          <!--需要判斷醫生有沒有回覆，才會顯示下面-->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
              	<b>醫生回覆</b>
              </h3>
            </div>
            <div class="panel-body">
              
            
            <!-- form -->
              
              <div class="form-group">
              <label for="text"></label>
              <div class="col-sm-12">
              <?php if(form_error('text')!=null): ?>
              <div class="alert alert-danger" role="alert">
                <?php echo form_error('text');?>
              </div>
              <?php endif ?>
              <textarea class="form-control" wrap="virtual|soft" rows="6" name="text"><?php echo $replys_item_mid['content'] ?></textarea><br />
              </div></div>

              <div class="form-group"> 
              <div class="col-sm-offset-1 col-sm-10"><center>
              <label for="mclass"></label>
              <input type="radio" name="secret" data-toggle="modal" data-target="#myModal" value="no" <?php if($news_item_id['secret']==0) echo "checked";?>> 一般留言(若勾選此欄位，內容將會被看見)&nbsp;&nbsp;
              <input type="radio" name="secret" value="yes" <?php if($news_item_id['secret']==1) echo "checked";?>> 私密留言(以Email形式回覆)</center>
              </div></div>

              <div class="form-group"> 
              <div class="col-sm-offset-1 col-sm-10"><center>
              <br>
              <button type="submit" name="submit" class="btn btn-default btn-lg" >更新留言 & 送出回覆</button></center></div></div>

              <!-- Modal -->
              <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h3 class="modal-title"><b>即將回覆一則留言</b></h3>
                    </div>
                    <div class="modal-body">
                      <h3><p>若將「私密留言」改為「一般留言」，</p>
                      <p>請注意本則 留言、回覆及姓氏 將會被大眾看見。</p></h3>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
                    </div>
                  </div>

                </div>
              </div>

            </form>
            </div>
          </div>
          <?php endif; ?>

       </div>

      <hr>
      
      <footer>
        <p><strong><center>&copy; 2015 Bestivf, Inc.</center></strong></p>
      </footer>

    </div> <!-- /container -->