    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <?php if($this->session->userdata('session_id')== null): //沒登錄?>
      <div class="jumbotron">
        <h1>留言板 - 管理區</h1>
        <p>歡迎來到中心留言板，您可以在這找到常見問題或提問留言，我們將盡速回覆您。</p>
        <p>
          <a class="btn btn-lg btn-warning" href="../../../index.php/pages/create/message" role="button">我要留言 &raquo;</a>
        </p>
      </div>
      <?php elseif($this->session->userdata('session_id')!= null): //已登錄?>
      <div class="well">
        <h2>留言板 - 管理區</h2>
      </div>
      <?php endif;?>

<?php if (!empty($news_item_common)): ?>
<div class="page-header">
        <h1><span class="label label-warning">常見問題</span></h1>
      </div>

<?php if($this->session->userdata('session_id')== null): //沒登錄?>
      <div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
              </tr>
            </thead>
            <tbody>
      <?php foreach ($news_item_common as $key => $rows): ?>
              <tr>
                <td class="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-7"><a href="../../../index.php/pages/view/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-2"><?php 
                  if($rows['secret']==0){
                    echo mb_substr($rows['name'], 0,1,"utf-8")." 先生/小姐";
                  }
                  else {
                    echo "匿名 先生/小姐";
                  }
                ?></td>
                <td class="col-md-2"><?php echo $rows['time'] ?></td>
              </tr>
            <?php endforeach ?>
            </tbody>
          </table>
       </div>

<?php elseif($this->session->userdata('session_id')!= null): //有登錄?>
      <div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
                <th>類型</th>
                <th>是否回覆</th>
                <th>刪除?</th>
              </tr>
            </thead>
            <tbody>
      <?php foreach ($news_item_common as $key => $rows): ?>
              <tr>
                <td class="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-6"><a href="../../../index.php/pages/adminview/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-1"><?php echo $rows['name']; ?></td>
                <td class="col-md-2"><?php echo $rows['time']; ?></td>
                <td class="col-md-1">
                  <?php if($rows['secret']==0) {
                      echo "公開留言";
                    }
                    else if($rows['secret']==1) {
                      echo "私密留言";
                    }
                  ?>
                </td>
                <td class="col-md-1"><?php 
                  echo (isset($replys_item_common_mid[$key])) ?  '<span class="label label-default">已回覆</span>' : '<a href="../../../index.php/pages/adminview/'.$rows['id'].'" class="label label-danger">未回覆</a>';  
                ?></td>
                <td class="col-md-1"><span class="glyphicon glyphicon-trash" aria-hidden="true" data-toggle="modal" data-target="#myModal<?php echo $rows['id'] ;?>"></span></td>
              </tr>
              <!-- Modal -->
              <div id="myModal<?php echo $rows['id'] ;?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h3 class="modal-title"><b>即將刪除一則留言</b></h3>
                    </div>
                    <div class="modal-body">
                      <h3><p><?php echo "欲刪除 \"".$rows['title']."\" 此一留言";?></p>
                      <br>
                      <p>若刪除後 ，留言內容將完全刪除</p>
                      <p>請確認是否要刪除?</p></h3>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
                      <a href="../delete/<?php echo $rows['id'] ?>" class="btn btn-success">確認</a>
                    </div>
                  </div>

                </div>
              </div>
            <?php endforeach ?>
            </tbody>
          </table>
       </div>
<?php endif;?>
<?php endif;?>
    

      <div class="page-header">
        <h1><span class="label label-info">一般留言</span></h1>
      </div>
      <ul class="nav nav-tabs" role="tablist">
        <?php 
          $str = explode("/",$_SERVER["REQUEST_URI"]);
          //echo $str[5]."<br>";
          $str = explode("-",$str[5]);
          //echo $str[0];
        ?>
        <li role="presentation" <?php if ($str[0]==="A") echo "class=\"active\""; ?>><a href="../adminpage/A-0">全部</a></li>
        <li role="presentation" <?php if ($str[0]==="B") echo "class=\"active\""; ?>><a href="../adminpage/B-0">分類1</a></li>
        <li role="presentation" <?php if ($str[0]==="C") echo "class=\"active\""; ?>><a href="../adminpage/C-0">分類2</a></li>
        <li role="presentation" <?php if ($str[0]==="D") echo "class=\"active\""; ?>><a href="../adminpage/D-0">分類3</a></li>
        <li role="presentation" <?php if ($str[0]==="E") echo "class=\"active\""; ?>><a href="../adminpage/E-0">分類4</a></li>
        <li role="presentation" <?php if ($str[0]==="F") echo "class=\"active\""; ?>><a href="../adminpage/F-0">分類5</a></li>
      </ul>
<?php if($this->session->userdata('session_id')== null): //沒登錄?>
    <div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($news_item_normal_by_pages as $key => $rows): ?>
              <tr>
                <td class="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-7"><a href="../../../index.php/pages/view/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-2"><?php 
                  if($rows['secret']==0){
                    echo mb_substr($rows['name'], 0,1,"utf-8")." 先生/小姐";
                  }
                  else {
                    echo "匿名 先生/小姐";
                  }
                ?></td>
                <td class="col-md-2"><?php echo $rows['time'] ?></td>
              </tr>
            <?php endforeach ?>
            </tbody>
          </table>

      <?php //CI的分頁系統
        if (isset($pagination)){
            echo $pagination;
          }
      ?>

       </div>
<?php elseif($this->session->userdata('session_id')!= null): //有登錄?>
<div><!--class="col-md-6"-->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>標題</th>
                <th>提問者</th>
                <th>日期</th>
                <th>類型</th>
                <th>是否回覆</th>
                <th>刪除?</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($news_item_normal_by_pages as $key => $rows): ?>
              <tr>
                <td class="width:4%"><?php echo $key+1 ?></td>
                <td class="col-md-6"><a href="../../../index.php/pages/adminview/<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></a></td>
                <td class="col-md-1"><?php echo $rows['name']; ?></td>
                <td class="col-md-2"><?php echo $rows['time']; ?></td>
                <td class="col-md-1">
                  <?php if($rows['secret']==0) {
                      echo "公開留言";
                    }
                    else if($rows['secret']==1) {
                      echo "私密留言";
                    }
                  ?>
                </td>
                <td class="col-md-1"><?php 
                  echo (isset($replys_item_mid[$key])) ?  '<span class="label label-default">已回覆</span>' : '<a href="../../../index.php/pages/adminview/'.$rows['id'].'" class="label label-danger">未回覆</a>'; 
                ?></td>
              <td class="col-md-1"><span class="glyphicon glyphicon-trash" aria-hidden="true" data-toggle="modal" data-target="#myModal<?php echo $rows['id'] ;?>"></span></td>
              </tr>
              <!-- Modal -->
              <div id="myModal<?php echo $rows['id'] ;?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h3 class="modal-title"><b>即將刪除一則留言</b></h3>
                    </div>
                    <div class="modal-body">
                      <h3><p><?php echo "欲刪除 \"".$rows['title']."\" 此一留言";?></p>
                      <br>
                      <p>若刪除後 ，留言內容將完全刪除</p>
                      <p>請確認是否要刪除?</p></h3>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
                      <a href="../delete/<?php echo $rows['id'] ?>" class="btn btn-success">確認</a>
                    </div>
                  </div>

                </div>
              </div>
            <?php endforeach ?>
            </tbody>
          </table>

      <?php //CI的分頁系統
        if (isset($pagination)){
            echo $pagination;
          }
      ?>

       </div>
<?php endif;?>

      <hr>
      
      <footer>
        <p><strong><center>&copy; 2015 Bestivf, Inc.</center></strong></p>
      </footer>

    </div> <!-- /container -->