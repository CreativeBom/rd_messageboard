<!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../../">Bestivf</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?php 
              $str = explode("/",$_SERVER["REQUEST_URI"]);
            ?>
            <li <?php if ($str[0]==="") echo "class=\"active\""; ?>><a href="/rd_messageboard/index.php">留言板</a></li>
            <li <?php if ($str[0]==="message") echo "class=\"active\""; ?>><a href="index.php/pages/create/message">我要留言</a></li>
            <!--li><a href="#about">About</a></li-->
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <?php 
              $private_session_id = $this->session->userdata('session_id');
              if(empty($private_session_id)):?>
              <li><a href="index.php/pages/admin/login">管理者登入</a></li>
            <?php elseif(!empty($private_session_id)):?>
              <li><a>歡迎 <?php echo $this->session->userdata('session_id');?></a></li>
              <li><a href="index.php/pages/admin/">管理選單</a></li>
              <li><a href="index.php/pages/admin/logout">登出</a></li>
            <?php endif;?>
            <!--li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>