<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News_model extends CI_Model 
{

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_all_normal_news($mclass)
	{
		if($mclass == "all"){
			$query = $this->db->get_where('rd_messageboard', array('secret' => 0));//'mclass' => 'normal', 
		}
		else {
			$query = $this->db->get_where('rd_messageboard',array('mclass' => $mclass , 'secret' => 0));
		}
		return $query->result_array();//如果只取數量，這方法要改。
	}

	public function get_all_admin_normal_news($mclass)
	{
		if($mclass == "all"){
			$query = $this->db->get_where('rd_messageboard');//, array('mclass' => 'normal')
		}
		else {
			$query = $this->db->get_where('rd_messageboard',array('mclass' => $mclass));//, array('mclass' => 'normal')
		}
		return $query->result_array();//如果只取數量，這方法要改。
	}
	
	public function get_news_all_mclass($slug = FALSE, $limit, $offset)
	{
		if ($slug == "all")
		{
			$query = $this->db->order_by("id", "desc");
			$query = $this->db->get_where('rd_messageboard', array('secret' => 0), $limit, $offset);
			return $query->result_array();
		}
		$query = $this->db->order_by("id", "desc");
		$query = $this->db->get_where('rd_messageboard', array('mclass' => $slug,'secret' => 0), $limit, $offset);
		return $query->result_array();
	}

	public function get_news_all($limit, $offset)
	{
		
		$query = $this->db->order_by("id", "desc"); 
		$query = $this->db->get_where('rd_messageboard', array('secret' => 0), $limit, $offset);//'mclass' => $slug,
		
		return $query->result_array();
	}

	public function get_news_mclass($slug = FALSE, $limit, $offset)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('rd_messageboard');
			return $query->result_array();
		}
		
		if($slug=="common"){
			$query = $this->db->order_by("id", "desc"); 
			$query = $this->db->get_where('rd_messageboard', array('mclass' => $slug), $limit, $offset);
		}
		else {
			$query = $this->db->order_by("id", "desc"); 
			$query = $this->db->get_where('rd_messageboard', array('mclass' => $slug, 'secret' => 0), $limit, $offset);
		}
		return $query->result_array();
	}

	public function get_news_id($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('rd_messageboard');
			return $query->result_array();
		}
		
		$query = $this->db->get_where('rd_messageboard', array('id' => $slug));
		return $query->row_array();
	}

	public function get_client_ip(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
		   $myip = $_SERVER['HTTP_CLIENT_IP'];
		}else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		   $myip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
		   $myip= $_SERVER['REMOTE_ADDR'];
		}
		return $myip;
	}

	public function set_news()
	{
		//$k = $_GET['k'];  
	    //$t = $_GET['t'];  
	    //防刷新时间  
	    $allowTime = 100;//100秒?
	    $ip = $this->get_client_ip();  
	    $allowT = md5($ip);  //. $k . $t
	    if (!isset($_SESSION[$allowT])) {  
	       $refresh = true;  
	       $_SESSION[$allowT] = time();  
	    } elseif (time() - $_SESSION[$allowT] > $allowTime) {  
	       $refresh = true;  
	       $_SESSION[$allowT] = time();  
	    } else {  
	       $refresh = false;  
	    }
	    // echo $allowT."<br>";
	    // echo time()."<br>";
	    // echo $_SESSION[$allowT]."<br>";
	    // echo date('Y-m-d', time());
	    // echo time() - $_SESSION[$allowT]."<br>";

	    if($refresh){
	    	//echo "Refresh";
			$this->load->helper('url');
			$news_secret=$this->input->post('secret');
			if(empty($news_secret)){
				$secret = 0;
			}
			else {
				$secret = $this->input->post('secret');
			}
			echo $this->input->post('mcategory');
			$data = array(
				'mclass' => $this->input->post('mcategory'),//'normal',
				//'mcategory' => $this->input->post('mcategory'),
				'name' => $this->input->post('name'),
				'phone' => $this->input->post('phone'),
				'email' => $this->input->post('email'),
				'title' => $this->input->post('title'),
				'content' => $this->input->post('text'),
				'secret' => $secret
			);
			if($this->input->post('captcha') == $this->session->userdata('captcha')){
				$this->db->insert('rd_messageboard', $data);
				unset($data);
				return TRUE;
			}
		}
		return FALSE;
	}

	public function delete_news($id)
	{
		return $this->db->delete('rd_messageboard', array('id' => $id)); 
	}

	public function RD_login(){
		$success = FALSE;
		$account=$this->input->post('account');
		$pw=$this->input->post('pw');
		$query = $this->db->get_where('rd_member', array('account' => $account), 1);
		$data['user']=$query->result_array();
		if(!empty($data['user'][0]['pw'])){
			if($data['user'][0]['pw']==md5($pw)){
				$success = $data['user'][0]['name'];
			}
		}		
		else {
			$success = Null;//FALSE;
		}
		return $success;
	}

	public function get_news_admin_mclass($slug = FALSE, $limit, $offset)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('rd_messageboard');
			return $query->result_array();
		}
		$query = $this->db->order_by("id", "desc");
		$query = $this->db->get_where('rd_messageboard', array('mclass' => $slug), $limit, $offset);
		return $query->result_array();
	}

	public function get_news_admin_all($mclass, $limit, $offset)
	{
		if($mclass=="all"){
			$query = $this->db->order_by("id", "desc");
			$query = $this->db->get('rd_messageboard', $limit, $offset);
		}
		else {
			$query = $this->db->order_by("id", "desc");
			$query = $this->db->get_where('rd_messageboard', array('mclass' => $mclass), $limit, $offset);
		}
		
		return $query->result_array();
	}

	public function set_reply($mid)
	{
		if(empty($mid)){
			return FALSE;//這樣寫不知道好不好
		}

		$secret = 0 ;
		if($this->input->post('secret')=="yes"){
			$secret = 1;
		}
		else if($this->input->post('secret')=="no"){
			$secret = 0;
		}
		//if($this->input->post('mclass')=="common"){
			$md_data = array(
               'mclass' => $this->input->post('mcategory'),//mclass
               'secret' => $secret,
               'content' => $this->input->post('news_content')
            );

			$this->db->where('id', $mid);
			$this->db->update('rd_messageboard', $md_data); 
		//}

		$this->load->helper('url');
		$data = array(
			'mb_id' => $mid,
			'content' => $this->input->post('text')
		);
		
		return $this->db->insert('rd_messagereply', $data);
	}

}