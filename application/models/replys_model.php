<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Replys_model extends CI_Model 
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_replys_mid($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('rd_messagereply');
			return $query->result_array();
		}
		$query = $this->db->order_by("id", "desc");
		$query = $this->db->get_where('rd_messagereply', array('mb_id' => $slug), 1, 0);
		return $query->row_array();
	}
}