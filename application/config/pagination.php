<?php
		//這邊需要設定
		
		//
		$config['per_page'] = 10; //每頁顯示的筆數
		$config['num_links'] = 2;
		$config['full_tag_open'] = '<ul class="pager">';
		
		$config['first_tag_open'] = '<li>';
		$config['first_link'] = '最先';
		$config['first_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = '前一頁';//&lt;
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a><b>';
		$config['cur_tag_close'] = '</li></a></b>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = '下一頁';//&gt;
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_link'] = '最後';
		$config['last_tag_close'] = '</li>';
		
		$config['full_tag_close'] = '</ul>';