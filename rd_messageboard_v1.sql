-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2016-01-17 17:12:21
-- 伺服器版本: 10.1.9-MariaDB
-- PHP 版本： 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `rd_messageboard`
--

-- --------------------------------------------------------

--
-- 資料表結構 `rd_member`
--

CREATE TABLE `rd_member` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `account` varchar(20) NOT NULL,
  `pw` varchar(41) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `rd_messageboard`
--

CREATE TABLE `rd_messageboard` (
  `id` int(11) NOT NULL,
  `mclass` varchar(32) NOT NULL,
  `title` varchar(128) NOT NULL,
  `content` text NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `secret` tinyint(1) DEFAULT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `rd_messageboard`
--

INSERT INTO `rd_messageboard` (`id`, `mclass`, `title`, `content`, `name`, `email`, `phone`, `secret`, `time`) VALUES
(1, 'normal', '想請問試管嬰兒和精卵輸卵管植入術有何不同？', '如題:想請問試管嬰兒和精卵輸卵管植入術有何不同？', '王大大', 'abc@123.com', '0987654321', 1, '2016-01-16 17:01:01'),
(2, 'common', '什麼情況是需要進入試管嬰兒治療?', ':)', '林小小', 'qwe@gmail.com', '0987654333', 0, '2016-01-16 20:46:55'),
(3, 'normal', 'XD', 'XD', 'XD', 'XD', 'D', 0, '2016-01-17 02:25:22'),
(4, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:25:32'),
(5, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:26:00'),
(6, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:27:58'),
(7, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:28:22'),
(8, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:29:58'),
(9, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:31:57'),
(10, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:32:29'),
(11, 'normal', 'XD', 'XD', 'XD', 'XD', 'XD', 0, '2016-01-17 02:33:01'),
(12, 'normal', '感謝醫生的照顧', '真的很開心可以遇到像你這麼好的醫生', '王大蠻', 'zxc@gmail.com', '0912345678', 0, '2016-01-17 02:38:04'),
(13, 'normal', '感謝醫生的照顧2', ':) thx!', '王大蠻', 'zxc@gmail.com', '0912345678', 0, '2016-01-17 02:40:06');

-- --------------------------------------------------------

--
-- 資料表結構 `rd_messagereply`
--

CREATE TABLE `rd_messagereply` (
  `id` int(11) NOT NULL,
  `mb_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `rd_messagereply`
--

INSERT INTO `rd_messagereply` (`id`, `mb_id`, `content`, `time`) VALUES
(1, 1, '記得好好休息。', '2016-01-16 14:25:26');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `rd_member`
--
ALTER TABLE `rd_member`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `rd_messageboard`
--
ALTER TABLE `rd_messageboard`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `rd_messagereply`
--
ALTER TABLE `rd_messagereply`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `rd_member`
--
ALTER TABLE `rd_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `rd_messageboard`
--
ALTER TABLE `rd_messageboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- 使用資料表 AUTO_INCREMENT `rd_messagereply`
--
ALTER TABLE `rd_messagereply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
